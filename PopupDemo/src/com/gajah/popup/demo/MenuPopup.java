/**
 * Copyright 2012 Gajah Media Pte,Ltd

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package com.gajah.popup.demo;

import android.app.Activity;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

// App List Menu
public class MenuPopup implements OnKeyListener {
	public PopupWindow demopopup;
	private Activity mContext;
	private ListView mListview;
	private View inflateView;;
	String[] mData = { "Option 1", "Option 2", "Option 3" };
	public final String TITLE = "title";
	public final String CONTENT = "content";

	public static final int OPTION_3 = 0;
	public static final int OPTION_2 = 1;
	public static final int OPTION_1 = 2;
	public static final int FEEDBACK = 3;

	public MenuPopup(Activity context) {
		mContext = context;
		LayoutInflater inflate = LayoutInflater.from(mContext);
		inflateView = inflate.inflate(R.layout.menu_window, null);
		mListview = (ListView) inflateView.findViewById(R.id.listview_popup);
		demopopup = new PopupWindow(inflateView, 300, LayoutParams.WRAP_CONTENT);
		demopopup.setFocusable(true);
	}

	public void show(View parent, int x, int y) {

		ArrayAdapter<String> sta = new ArrayAdapter<String>(mContext,
				R.layout.menu_list_item, R.id.text, mData);
		mListview.setAdapter(sta);
		mListview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		mListview.clearChoices();
		mListview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				setItemChecked(position);
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,
						KeyEvent.KEYCODE_DPAD_CENTER));
			}
		});
		mListview.setOnKeyListener(this);
		demopopup.showAtLocation(parent, Gravity.NO_GRAVITY, x, y);
	}

	public boolean isShowing() {
		if (demopopup != null)
			return demopopup.isShowing();
		return false;
	}

	public void dismiss() {
		if (demopopup != null)
			demopopup.dismiss();

	}

	public void setItemChecked(int position) {
		mListview.setItemChecked(position, true);
	}

	public boolean dispatchKeyEvent(KeyEvent event) {
		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK
				&& event.getAction() == KeyEvent.ACTION_DOWN) {
			dismiss();
			return true;
		} else if (event.getKeyCode() == KeyEvent.KEYCODE_MENU
				&& event.getAction() == KeyEvent.ACTION_DOWN) {
			dismiss();
			return true;
		} else if ((event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER)
				&& event.getAction() == KeyEvent.ACTION_DOWN) {
			int position = mListview.getCheckedItemPosition();
			if (position < 0)
				position = (int) mListview.getSelectedItemId();

			switch (position) {
			case OPTION_1:
				dismiss();
				Toast.makeText(mContext, "Option 1 is clicked",
						Toast.LENGTH_SHORT).show();
				break;
			case OPTION_2:
				dismiss();
				Toast.makeText(mContext, "Option 2 is clicked",
						Toast.LENGTH_SHORT).show();
				break;
			case OPTION_3:
				dismiss();
				Toast.makeText(mContext, "Option 3 is clicked",
						Toast.LENGTH_SHORT).show();
				break;

			}
			return true;
		}
		return false;
	}

	public boolean onKey(View v, int keyCode, KeyEvent event) {
		return dispatchKeyEvent(event);
	}
}
