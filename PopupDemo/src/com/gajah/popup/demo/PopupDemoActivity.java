/**
 * Copyright 2012 Gajah Media Pte,Ltd

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.gajah.popup.demo;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;

public class PopupDemoActivity extends Activity {
    /** Called when the activity is first created. */
	
	MenuPopup demopw;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.main);
    }
    
    @Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		if (demopw != null && demopw.isShowing()) {
			return demopw.dispatchKeyEvent(event);
		} else if (event.getKeyCode() == KeyEvent.KEYCODE_MENU
				&& event.getAction() == KeyEvent.ACTION_DOWN) {
			if (demopw == null) {
				demopw = new MenuPopup(this);

				demopw.show(this.findViewById(R.id.linearlayout), 3, 3);
			} else {
				demopw.show(this.findViewById(R.id.linearlayout), 3, 3);
			}
			return true;
		}
		return super.dispatchKeyEvent(event);
	}
}